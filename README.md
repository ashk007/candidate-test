# noissue Candidate Exercise

Welcome to the noissue candidate exercise!
You would have been asked to complete a technical exercise as part of the recruitment process.  
There are different exercises on different branches of this repository.  
Please check out the branch you have been asked to complete and check out the README.md on that branch.

Exercise List:

- [Frontend exercise](https://bitbucket.org/noissueteam/candidate-test/src/noissue-fed-exercise/)
- [Full Stack exercise](https://bitbucket.org/noissueteam/candidate-test/src/noissue-full-stack-exercise/)
